package loc;

public class App 
{
	public int numberOfLOC(final String string) {
		int locLines = 0;
		boolean isNotMultiLineComment = true;
		final String[] lines = string.split("\r\n");
		for (final String line : lines) {
			if (isOpenMultiLineComment(line))
				isNotMultiLineComment = false;

			if (isNotMultiLineComment && isLOC(line)) {
				++locLines;
			}

			if (isCloseMultiLineComment(line))
				isNotMultiLineComment = true;
		}
		return locLines;
	}

	public boolean isLOC(final String line) {
		return !isWhitespaceLine(line) && !isOneLineComment(line);
	}

	public boolean isWhitespaceLine(final String string) {
		return string.isBlank();
	}

	public boolean isOneLineComment(final String string) {
		return string.contains("//");
	}

	public boolean isOpenMultiLineComment(final String string) {
		return string.contains("/*");
	}

	public boolean isCloseMultiLineComment(final String string) {
		return string.contains("*/");
	}

	public int detectWhitespaceLines(String string) {
		char[] characters = string.toCharArray();
		boolean previousN=false;
		boolean previousR=false;
		int whitespaceLines=0;
		for(char c : characters){

			if(c=='\n' && previousN && previousR)
				++whitespaceLines;

			if(c=='\n'){
				previousN=true;
			}
			else if(c=='\r'){
				previousR=true;
			}
			else{
				previousN=false;
				previousR=false;
			}
		}
		return whitespaceLines;
	}
}
