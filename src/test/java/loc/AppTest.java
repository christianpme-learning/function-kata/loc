package loc;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

public class AppTest {

    String string="";

    @Before
    public void initialize(){
        
        try {
            string = new String(Files.readAllBytes(Paths.get("src/resources/codesample.cs")));
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void numberOfLOCTest(){

        App app = new App();
        int actual = app.numberOfLOC(string);

        assertTrue(actual==14);
    }

    @Test
    public void isLOCTest(){

        App app = new App();
        boolean isLOC = app.isLOC("class HelloGeeks {  ");

        assertTrue(isLOC);
    }

    @Test
    public void isWhitespaceLineTest(){

        App app = new App();
       
        assertTrue(app.isWhitespaceLine(""));
        assertTrue(app.isWhitespaceLine(" "));
        assertTrue(app.isWhitespaceLine("           "));
    }

    @Test
    public void isOneLineCommentTest(){

        App app = new App();
        boolean actual = app.isOneLineComment("    // Single Line Comment -- Function to print Message");

        assertTrue(actual);
    }

    @Test
    public void isOpenMultiLineCommentTest(){

        App app = new App();
        boolean actual = app.isOpenMultiLineComment("        /* Multiline Comment -- ");

        assertTrue(actual);
    }

    @Test
    public void isCloseMultiLineCommentTest(){

        App app = new App();
        boolean actual = app.isCloseMultiLineComment("           value to it*/");

        assertTrue(actual);
    }
}
