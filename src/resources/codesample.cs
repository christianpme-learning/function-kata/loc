using System; 
  
namespace HelloGeeksApp { 
      
class HelloGeeks {  
  
    // Single Line Comment -- Function to print Message 
    public static void Message(string message) 
    { 
        Console.WriteLine(message); 
    } 
      
    // Main function 
    static void Main(string[] args) 
    { 
          
        /* Multiline Comment -- 
           Define a variable of 
           string type and assign 
           value to it*/
        string msg = "GeeksforGeeks"; 
          
        // Calling function 
        Message(msg); 
    } 
} 
} 